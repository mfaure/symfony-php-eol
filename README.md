# Symfony-PHP-EOL

Map of respective end-of-life for releases of Symfony, PHP, Debian, Ubuntu

## End of life (EOL)

PHP:

* [PHP Supported versions](https://www.php.net/supported-versions.php)

Symfony:

* [Symfony Releases](https://symfony.com/releases)
* [Symfony pre-requisites](https://symfony.com/doc/master/setup.html#technical-requirements)
    * Symfony 4.4 => PHP 7.1
    * Symfony 5.0 => PHP 7.2.5

Ubuntu:

* [Ubuntu lifecycle](https://ubuntu.com/about/release-cycle)
* [Ubuntu list of releases (past + future)](https://wiki.ubuntu.com/Releases)
* [PHP 7.4 in 20.04](https://discourse.ubuntu.com/t/php-7-4-in-focal/15000)
* [Upcoming PHP 7.4 transition](https://lists.ubuntu.com/archives/ubuntu-devel/2020-February/040901.html)

Debian:

* [Debian Releases official](https://www.debian.org/releases/)
* [Wiki Debian releases Production](https://wiki.debian.org/DebianReleases#Production_Releases)
* [PHP 7.3 in Debian 10 Buster](https://wiki.debian.org/DebianBuster)

## Result of combinations

| Distro family | Distro version    | Shipped with PHP  | Min Symfony version   | Until  |
|---------------|------------------:|------------------:|----------------------:|-------:|
| Ubuntu        | 18.04             | 7.2               | 4.4                   | 2023   |
| Ubuntu        | 20.04             | 7.4               | 4.4                   | 2025   |
| Debian        | *Stretch*  9      | 7.0               | NA                    | ~2020  |
| Debian        | *Buster*  10      | 7.3               | 4.4                   | ~2022  |
